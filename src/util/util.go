// Programmed by Rimon Oz.
package util

import (
	"os"
	"net"
	"encoding/json"
	"configuration"
	"strconv"
	"fmt"
	"runtime"
	"io/ioutil"
)

// This function loads configuration from disk.
func LoadConfiguration(location string) *configuration.Configuration {
	file, _ := os.Open("conf/periflash.json")
	decoder := json.NewDecoder(file)
	configuration := configuration.Configuration{}
	err := decoder.Decode(&configuration)
	Check(err)
	return &configuration
}

// This function retrieves the local IP as a net.IP.
func GetLocalIP() net.IP {
	addresses, err := net.InterfaceAddrs()
	Check(err)
	// foreach local address
	for _, address := range addresses {
		// if the address is valid and non-loopback
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			// if it can be turned into ipv4 without breaking
			if ipnet.IP.To4() != nil {
				// return it
				return ipnet.IP
			}
		}
	}
	// otherwise no local address :(
	return nil
}

// This function performs simple error handling.
func Check(err error) {
	// if there was an error
	if err != nil {
		pc, fn, line, _ := runtime.Caller(1)
		os.Stderr.WriteString(fmt.Sprintf(
			"[error] in %s[%s:%d] %v",
			runtime.FuncForPC(pc).Name(), fn, line, err))
		// and quit
		os.Exit(1)
	}
}

func IntArrayToVersionString(array []int32) string {
	versionNumber := "v"
	for _, number := range array {
		versionNumber += strconv.Itoa(int(number)) + "."
	}
	return versionNumber[:len(versionNumber)-1]
}

func StringArrayToVersionString(array []string) string {
	versionNumber := "v"
	for _, number := range array {
		versionNumber += number + "."
	}
	return versionNumber[:len(versionNumber)-1]
}

func ToVersionInt32Array(array []string) []int32 {
	var versions []int32

	for _, versionString := range array {
		if result, err := strconv.Atoi(versionString); err == nil {
			versions = append(versions, int32(result))
		}
	}

	return versions
}

func GetDirectoryListing(directory string) []string {
	var listing []string
	files, _ := ioutil.ReadDir(directory)
	for _, f := range files {
		listing = append(listing, f.Name())
	}
	return listing
}

