// Programmed by Rimon Oz.
package server

import (
	"net"
	"peridaemon"
	"protocol"
	"github.com/golang/protobuf/proto"
	"configuration"
	"util"
	"strconv"
	"strings"
	"time"
	"os"
	"io"
)

type PeriServer struct {
	*peridaemon.PeriDaemon
}

// This function creates a new periflash server.
func NewServer(configuration configuration.Configuration, quit chan int, terminal chan string) *PeriServer {
	multicast, err := net.ResolveUDPAddr("udp",
		configuration.Multicast.Address + ":" + strconv.Itoa(configuration.Multicast.Port))
	util.Check(err)
	// create the daemon
	daemon := &peridaemon.PeriDaemon{
		Name:              configuration.Name,
		Type:              "server",
		LocalPort:         configuration.LocalPort,
		Multicast:         *multicast,
		Incoming:          make(chan *peridaemon.UDPMessage),
		Outgoing:          make(chan *protocol.Message),
		FirmwareDirectory: configuration.FirmwareDirectory,
	}
	// start the UDP listener and the UDP writer
	go daemon.Speak(quit)
	go daemon.Listen(quit)
	go daemon.Watch(quit)
	server := &PeriServer{daemon}
	// start the poller
	go server.PollMulticast(time.Duration(configuration.PollInterval), terminal)
	// return the server daemon
	return server
}

func (ps *PeriServer) Start(terminal chan string) {
	// send out a PING at startup
	terminal    <- "[->] Sending PING by broadcast."
	ps.Outgoing <- &protocol.Message{
		DType: protocol.Message_SERVER,
		Code:  protocol.Message_PING,
		Name:  ps.Name,
	}

	// keep looping
	for {
		// retrieve a message
		message := <- ps.Incoming
		// reserve the memory for the message
		command := &protocol.Message{}
		// attempt to unmarshal the message into command
		err := proto.Unmarshal(message.Content, command)

		// if there was an error
		if err != nil {
			// then report it and don't fail
			terminal <- "[<-] Received a malformed packet."
		// otherwise if it's a message sent by a client
		} else if command.DType == protocol.Message_CLIENT {
			// report the type of message and its origin
			terminal <- "[+]  " + command.Code.String() + " received from " +
				command.Name + " at " + message.Origin.IP.String()

			// based on the message type
			switch (command.Code) {
			// if it was a PONG, then the client is responsive
			case protocol.Message_PONG:
				// and we reply with a VERSIONS request
				terminal    <- "[->] Sending VERSIONS by broadcast."
				ps.Outgoing <- &protocol.Message{
					Code:   protocol.Message_VERSIONS,
					DType:  protocol.Message_SERVER,
					Name:   ps.Name,
					Target: command.Name,
				}
			// if it was a VERSIONS, then extract the version information
			// and respond with a list of possible updates.
			case protocol.Message_VERSIONS:
				// create an array of modules which need updating
				var needsUpdating []*protocol.ModuleVersion
				// for each module
				for index, element := range command.GetVersions() {
					// dump module information
					terminal <- "[!]  " + command.Name + " has module " + strconv.Itoa(index) +
						" named " + element.Name + " with flag: " +
						element.Flag.String() + " and version " +
						util.IntArrayToVersionString(element.Version)
					// for every possible match
					for _, module := range ps.Versions {
						// compare the names
						if module.Name == element.Name {
							// and make sure the version is higher
							if strings.Compare(
								util.IntArrayToVersionString(element.Version),
								util.IntArrayToVersionString(module.Version)) < 0 {
								// if so append, and skip the rest of the list
								needsUpdating = append(needsUpdating, module)
								break;
							}
						}
					}

				}

				// if there's any modules which need updating
				if len(needsUpdating) > 0 {
					terminal    <- "[->] Sending REQUEST_TO_UPDATE to " + command.Name
					// then send a request to update
					ps.Outgoing <- &protocol.Message{
						Code:     protocol.Message_REQUEST_TO_UPDATE,
						DType:    protocol.Message_SERVER,
						Name:     ps.Name,
						Target:   command.Name,
						Versions: needsUpdating,
					}
					// start a goroutine for sending the binaries
					go ps.SendFirmware(needsUpdating, message.Origin.IP.String(), terminal)
				}
			}
		}
	}
}


// Sends a list of firmware files over the network through TCP.
func (ps *PeriServer) SendFirmware(modules []*protocol.ModuleVersion, target string, terminal chan string) {
	// give the client a little while to warm up
	timer := time.NewTimer(1 * time.Second)
	<- timer.C
	// attempt to open a tcp connection
	connection, err := net.Dial("tcp", target + ":" + strconv.Itoa(ps.PeriDaemon.LocalPort))

	// if it succeeded
	if err == nil {
		// for every module which needs to be updated
		for _, module := range modules {

			// wrap a header so the client knows what it's getting
			data, err := proto.Marshal(module)
			if err == nil {
				// write the header
				connection.Write(data)
				// determine the filename
				filename := module.Name + "@" + util.IntArrayToVersionString(module.Version) + ".bin"

				// open the firmware file on disk locally
				file, err := os.Open(ps.FirmwareDirectory + filename)
				util.Check(err)
				// make sure to close the file even if we panic
				defer file.Close()
				// dump the file over TCP
				n, err := io.Copy(connection, file)
				util.Check(err)
				// report status
				terminal <- "[!]  " + strconv.Itoa(int(n)) + " bytes sent from " +
					filename + " to " + target + ":" + strconv.Itoa(ps.PeriDaemon.LocalPort)
				// close the file
				file.Close()
				// wait 5ms for next binary
				timer := time.NewTimer(10 * time.Millisecond)
				<-timer.C
			}
		}
		connection.Close()
	}
}

// Polls the multicast server at regular intervals.
func (ps *PeriServer) PollMulticast (interval time.Duration, terminal chan string) {
	for {
		timer := time.NewTimer(interval * time.Second)
		<-timer.C
		terminal <- "[->] Sending PING by broadcast."
		ps.Outgoing <- &protocol.Message{
			DType: protocol.Message_SERVER,
			Code:  protocol.Message_PING,
			Name:  ps.Name,
		}
	}
}