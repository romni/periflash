package peridaemon

import (
	"net"
	"util"
	"protocol"
	"github.com/golang/protobuf/proto"
	"time"
	"regexp"
	"os"
	"strconv"
	"strings"
)

// This struct represents a periflash-compatible networking client.
type PeriDaemon struct {
	Type              string
	Name              string
	Local             net.UDPAddr
	LocalPort         int
	Multicast         net.UDPAddr
	Incoming          chan *UDPMessage
	Outgoing          chan *protocol.Message
	FirmwareDirectory string
	Versions  	  []*protocol.ModuleVersion
}

// This struct represents a simple UDP message with content and an origin.
type UDPMessage struct {
	Origin  *net.UDPAddr
	Content []byte
}

// This method starts up the UDP listener.
func (ps *PeriDaemon) Listen(quit chan int) {
	localaddr, err := net.ResolveUDPAddr("udp4", ps.Multicast.IP.String() + ":" + strconv.Itoa(ps.Multicast.Port))
	util.Check(err)
	// join the multicast group on the provided net.UDPAddr
	conn, err := net.ListenMulticastUDP("udp4", nil, localaddr)
	util.Check(err)
	// keep looping
	for {
		// create a buffer
		buffer := make([]byte, 256)
		// read a packet
		size, source, err := conn.ReadFromUDP(buffer)
		util.Check(err)
		// cut out the data
		message := buffer[:size]
		// handle the message
		ps.Incoming <- &UDPMessage{Origin: source, Content: message}
	}
}

// This method starts up the UDP writer.
func (ps *PeriDaemon) Speak(quit chan int) {
	// resolve the address for the udp writer
	localAddress, err := net.ResolveUDPAddr("udp4", ps.Multicast.IP.String() + ":" +
		strconv.Itoa(ps.Multicast.Port))
	util.Check(err)
	// open the connection
	connection, err := net.DialUDP("udp4", nil, localAddress)
	for {
		// for every protobuf message which needs to be sent
		message := <- ps.Outgoing
		// marshal it
		data, err := proto.Marshal(message)
		util.Check(err)
		// and send it
		_, err = connection.Write(data)
	}
}

// This method starts the firmware directory watcher.
func (ps *PeriDaemon) Watch(quit chan int) {
	// this regexp matches firmware filenames.
	binaryFormat, err := regexp.Compile(`^(.*?)\@(?:v(\d+)\.(?:(\d)\.)+(\d+)).bin$`)
	util.Check(err)
	for {
		// for every firmware binary in the list
		var moduleVersions []*protocol.ModuleVersion
		for _, filename := range util.GetDirectoryListing(ps.FirmwareDirectory) {
			if binaryFormat.MatchString(filename) == true {
				// extract relevant details
				result := binaryFormat.FindStringSubmatch(filename)
				// open the file
				file, err := os.Open(ps.FirmwareDirectory + filename)
				util.Check(err)
				// retrieve file information
				fileInfo, err := file.Stat()
				util.Check(err)
				// check if the newest version is in the list
				var inserted bool
				for index, module := range moduleVersions {
					// if we have a matching module and a higher version number
					if (result[1] == module.Name) && (strings.Compare(
						util.IntArrayToVersionString(module.Version),
						util.StringArrayToVersionString(result[2:5])) < 0) {
						// then replace the module in the list
						inserted = true
						moduleVersions[index] = &protocol.ModuleVersion{
							Name:    result[1],
							Version: util.ToVersionInt32Array(result[2:5]),
							Flag:    protocol.ModuleVersion_MANAGED,
							Size:    int32(fileInfo.Size()),
						}
					}
				}
				// if the module wasn't there to begin with
				if inserted == false {
					// then append it to the list
					moduleVersions = append(moduleVersions, &protocol.ModuleVersion{
						Name:    result[1],
						Version: util.ToVersionInt32Array(result[2:5]),
						Flag:    protocol.ModuleVersion_MANAGED,
						Size:    int32(fileInfo.Size()),
					})
				}
			}
		}
		// update the list of firmware versions
		ps.Versions = moduleVersions
		// wait 5 seconds before resuming
		timer := time.NewTimer(5 * time.Second)
		<- timer.C
	}
}

