// Programmed by Rimon Oz.
package configuration

// This struct represents the JSON-configuration object structure in /conf/periflash.json
type Configuration struct {
	FirmwareDirectory string
	Name              string
	LocalPort         int
	PollInterval      int
	Multicast struct {
		Port    int
		Address string
	}
}
