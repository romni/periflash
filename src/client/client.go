// Programmed by Rimon Oz.
package client

import (
	"net"
	"peridaemon"
	"protocol"
	"github.com/golang/protobuf/proto"
	"configuration"
	"util"
	"strconv"
	"os"
	"io"
)

type PeriClient struct {
	*peridaemon.PeriDaemon
}

// This function creates a new periflash client.
func NewClient(configuration configuration.Configuration, quit chan int, terminal chan string) *PeriClient {
	multicast, err := net.ResolveUDPAddr("udp",
		configuration.Multicast.Address + ":" + strconv.Itoa(configuration.Multicast.Port))
	util.Check(err)
	// create the daemon
	daemon := &peridaemon.PeriDaemon{
		Name:              configuration.Name,
		Type:              "client",
		LocalPort:         configuration.LocalPort,
		Multicast:         *multicast,
		Incoming:          make(chan *peridaemon.UDPMessage),
		Outgoing:          make(chan *protocol.Message),
		FirmwareDirectory: configuration.FirmwareDirectory,
	}
	go daemon.Speak(quit)
	// start listening in a goroutine
	go daemon.Listen(quit)
	go daemon.Watch(quit)
	// return the server daemon
	return &PeriClient{daemon}
}


func (ps *PeriClient) Start(terminal chan string) {
	// while there's messages
	for {
		// read a message
		message := <-ps.Incoming

		// prepare some memory and unmarshal the message into a command
		command := &protocol.Message{}
		err := proto.Unmarshal(message.Content, command)

		// if there was an error
		if err != nil {
			// handle it gracefully
			terminal <- "[<-] Received a malformed packet."
		// otherwise listen for server messages
		} else if command.DType == protocol.Message_SERVER {
			// report the type of message
			terminal <- "[<-] " + command.Code.String() + " received from " +
				command.Name + " at " + message.Origin.IP.String()

			// depending on the message type
			switch (command.Code) {
			// if it was a PING
			case protocol.Message_PING:
				terminal    <- "[->] Broadcasting PONG by broadcast."
				// then broadcast a PONG
				ps.Outgoing <- &protocol.Message{
					Code:  protocol.Message_PONG,
					DType: protocol.Message_CLIENT,
					Name:  ps.Name,
				}
			// if it was a VERSIONS directed at this client
			case protocol.Message_VERSIONS:
				if command.Target == ps.Name {
					terminal <- "[->] Broadcasting VERSIONS information."
					// broadcast a list of versions
					ps.Outgoing <- &protocol.Message{
						Code:     protocol.Message_VERSIONS,
						DType:    protocol.Message_CLIENT,
						Name:     ps.Name,
						Versions: ps.Versions,
					}
				}
			// if it was a REQUEST_TO_UPDATE directed at this client
			case protocol.Message_REQUEST_TO_UPDATE:
				if command.Target == ps.Name {
					terminal <- "[+]  Server has the following (newer) firmware versions: "
					// report the modules which can be updated
					for _, element := range command.GetVersions() {
						terminal <- "     " + element.Name + "@" +
						util.IntArrayToVersionString(element.Version) +
						" flagged as " + element.Flag.String()
					}
					ps.ReceiveFirmware(len(command.GetVersions()), terminal)
				}

			}
		}
	}
}

// Receives a number of firmware binaries and saves them to storage.
func (ps *PeriClient) ReceiveFirmware(numberOfFiles int, terminal chan string) {
	// open a local tcp listener
	server, err := net.Listen("tcp", ":" + strconv.Itoa(ps.PeriDaemon.LocalPort))

	// if we succeeded
	if err == nil {
		// create a buffer
		data := make([]byte, 4096)
		// accept a connection
		connection, err := server.Accept()
		util.Check(err)

		// for every file
		for i := 1; i <= numberOfFiles; i = i + 1 {
			// read the header from the connection
			size, err := connection.Read(data)
			// if we succeeded
			if err == nil {
				// unpack the header
				protodata := &protocol.ModuleVersion{}
				err = proto.Unmarshal(data[0:size], protodata)
				// if that didn't succeed
				if err != nil {
					// report it
					terminal <- "[!]  Something weird happened"
				// otherwise
				} else {
					// determine the filename
					filename := protodata.Name + "@" +
						util.IntArrayToVersionString(protodata.Version) + ".bin"
					terminal <- "[!]  About to receive " + filename
					// create the file
					file, _ := os.Create(ps.FirmwareDirectory + filename)
					defer file.Close()

					// dump the binary into the file
					n, err := io.CopyN(file, connection, int64(protodata.Size))
					util.Check(err)
					// report status
					terminal <- "[+] " + strconv.Itoa(int(n)) +
						" bytes received and saved in " + filename
					file.Close()
				}
			}
		}
		connection.Close()
	}
	server.Close()
}