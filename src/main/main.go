// Programmed by Rimon Oz.
package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	"server"
	"client"
	"fmt"
	"util"
)

// configure command line arguments
var verboseFlag           = kingpin.Flag("verbose", "Verbose mode.").Short('v').Bool()
var serverFlag            = kingpin.Flag("server",  "Server mode." ).Short('s').Bool()
var clientFlag            = kingpin.Flag("client",  "Client mode." ).Short('c').Bool()
var nameFlag              = kingpin.Flag("name",    "Daemon name." ).Short('n').String()
var firmwareDirectoryFlag = kingpin.Flag("fwdir",   "Directory where the firmware is located").Short('f').String()

// Starts up periflash.
func main() {
	// parse the command line arguments
	kingpin.Parse()
	// load the configuration from disk
	configuration := util.LoadConfiguration("conf/periflash.json")
	// override cli args
	if *nameFlag != "" {
		configuration.Name = *nameFlag
	}
	if *firmwareDirectoryFlag != "" {
		configuration.FirmwareDirectory = *firmwareDirectoryFlag
	}

	// create a buffered stdio
	terminal := make(chan string)
	go handleTerminal(terminal)
	// create the channel on which to quit
	quit     := make(chan int)

	// if the server arg was given then
	if *serverFlag == true {
		periServer := server.NewServer(*configuration, quit, terminal)
		terminal <- "Server is starting."
		go periServer.Start(terminal)
	}
	// if the client arg was given then
	if *clientFlag == true {
		periClient := client.NewClient(*configuration, quit, terminal)
		terminal <- "Client is starting."
		go periClient.Start(terminal)
	}

	// and wait for a quit signal
	<-quit
}


// This function makes sure that stdout doesn't get too crowded.
func handleTerminal(terminal chan string) {
	// while there's input
	for {
		// read a line
		line := <-terminal
		// print it if necessary
		if *verboseFlag == true {
			fmt.Println(line)
		}
	}
}
