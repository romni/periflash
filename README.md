# periflash
> An IoT device discovery and firmware management tool

## How to build

Make sure you have `go>=v1.6` installed. Add the project's root to the `GOPATH` environment variable. 
Install the project's dependencies like this:

```
go get gopkg.in/alecthomas/kingpin.v2
go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
```

Next up, run the following command in the project's directory:

```
go build -o ./bin/periflash ./src/main/main.go
```

You can then run the program with:

```
./bin/periflash
```


## How to contribute

Contributions are always welcome. Make sure you read the [guidelines for contributing](CONTRIBUTING.md) first.

## License

I hereby waive all copyright and any related rights to this work to the extent possible under law.
All of the source code in this repository is part of the public domain.