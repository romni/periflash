#!/bin/bash
go build -o ./bin/periflash ./src/main/main.go
GOPATH=`pwd` GOOS=linux   GOARCH=amd64 CGO_ENABLED=0 go build -o ./bin/periflash_linux_amd64 ./src/main/main.go
GOPATH=`pwd` GOOS=darwin  GOARCH=amd64 CGO_ENABLED=0 go build -o ./bin/periflash_osx_amd64 ./src/main/main.go
GOPATH=`pwd` GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -o ./bin/periflash_win_amd64.exe ./src/main/main.go
GOPATH=`pwd` GOOS=windows GOARCH=386   CGO_ENABLED=0 go build -o ./bin/periflash_win_x86.exe ./src/main/main.go
