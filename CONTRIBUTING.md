# Contribution Guidelines

Please ensure your merge request adheres to the following guidelines:

- Search previous merge requests before making a new one, as yours may be a duplicate.
- Keep descriptions short and simple, but descriptive.
- Check your spelling and grammar.
- Make sure your text editor is set to remove trailing whitespace.

Thank you for contributing!